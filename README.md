# upload image to S3 bucket using API Gateway and Lambda

Project: Upload an image to a S3 Bucket through a POST REST call through API Gateway and Lambda function


![small](/images/project.png)

Steps:
1. Create an API with API Gateway
2. Create a Lambda Function
3. Create an IAM Role
4. Create a S3 bucket
5. Deploy the API
6. Test the API using Postman tool
7. Test the API using python program

**Create an API with API Gateway**

Create a new API for POST method using Amazon API Gateway. Navigate to API Gateway service and click in Create to launch a new API.

![small](/images/api1.PNG)

Choose the Protocol as REST and give a name for the new API.

![small](/images/api2.PNG)

We shall upload an image through a POST REST method. So now create a new resource with a POST Method.
Go to Actions and select Create Resource and give a name to it.

![small](/images/api3.PNG)

![small](/images/api4.PNG)

Create a POST under newly created Resoucre.

![small](/images/api5.PNG)

![small](/images/api6.PNG)

Our POST method is created and we can link an integration point with it. Here we can link our Lambda function.

![small](/images/api7.PNG)


**Create a Lambda Function**

Now create a Lambda function in Python 3.6 to upload the image file to S3. Copy the body from "lambda_function.py" and paste into code body.

![small](/images/lambda1.PNG)

![small](/images/lambda2.PNG)

![small](/images/lambda3.PNG)

**Create an IAM Role**

Create a new IAM role and give a name. Select two custome roles from the list for S3 full access and API-Gateway cloudwatch log.

![small](/images/iam.PNG)

Then assign the Role to Lambda funciton

![small](/images/lambda4.PNG)


**Create a S3 bucket**

Create a new S3 bucket using S3 service.

![small](/images/s3.PNG)


**Deploy the API**

Go back to API and select the POST method. Now configure Integration Request. Go to Mapping Template and set "application/json". The POST method will pass a json format body to the lambda function. Lambda function will receive the body as a dictionary object wrapped inside a key named "body".

**event = {"body" : "content coming from the POST method"}**

![small](/images/api8.PNG)

![small](/images/api9.PNG)

Now deploy the API

![small](/images/api10.PNG)

Create a new stage and give it a name, inside deployment state you can find the method call and related invoke URL as shown in picture.
![small](/images/api11.PNG)

![small](/images/api12.PNG)

Got to lambda function and copy the link

![small](/images/lambda5.PNG)

**Test the API using Postman tool**
Now everything is ready we can test the API and Lambda function. The POST method takes header Content-type as "application/json" and body as following:

{
    "filename":"test.png",
    "base64data" : "base64 encoded string of an image"
}

![small](/images/postman1.PNG)

**Test the API using python program**

Now we can create a python program to convert any image file to base64 string and put inside payload and directy call the endpoint using POST method using Request library. Check the code in imageUpload.py.
