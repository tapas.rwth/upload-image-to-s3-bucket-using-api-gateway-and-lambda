#This is created by Tapas Jana
#This is a POST method call

import requests
import json
import base64
import os


def uploadFiletoS3(filepath,url):

    #encoding image to base64 string
    with open(filepath, "rb") as image_file:
        encoded_string = base64.b64encode(image_file.read())
    encoded_data = encoded_string.decode('utf-8')
    file_name = os.path.basename(filepath)

    #creating payload
    payload_dict = dict({"filename": file_name, "base64data": encoded_data})
    payload = json.dumps(payload_dict)
    headers = {
        'Content-Type': 'application/json'
    }
    response = requests.request("POST", url, headers=headers, data=payload)
    return response.text

if __name__ == '__main__':
    file = "path to image file"
    url = "API endpoint URL"
    response = uploadFiletoS3(file , url)
    print(response)
