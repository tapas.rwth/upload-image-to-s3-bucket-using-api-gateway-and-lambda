import json
import base64
import boto3

def lambda_handler(event, context):
    s3 = boto3.client('s3')
    data = event['body']
    file_name = data['filename']
    image_string = data['base64data']
    
    #decode image
    decoded_data = base64.b64decode(image_string)
    
    #file size calculator
    nbytes = (len(image_string) * 3) / 4 - image_string.count('=', -2)
    suffixes = [ 'B', 'KB', 'MB', 'GB', 'TB', 'PB' ]
    i = 0
    while nbytes >= 1024 and i < len(suffixes) - 1:
        nbytes /= 1024.
        i += 1
    f = ('%.2f' % nbytes).rstrip('0').rstrip('.')
    file_size = (f +" "+suffixes[i])
    
    #upload to s3 bucket
    bucket_name="s3 bucket name"
    s3.put_object(Bucket=bucket_name, Key=file_name, Body=decoded_data)
    
    #get image url
    bucket_location = boto3.client('s3').get_bucket_location(Bucket=bucket_name)
    object_url = "https://s3-{0}.amazonaws.com/{1}/{2}".format(bucket_location['LocationConstraint'],bucket_name,file_name)
    
    return {
		'statusCode': 200,
		'message': 'upload successful',
		"file size": file_size,
        "file name" : file_name,
		"image url": object_url
	}